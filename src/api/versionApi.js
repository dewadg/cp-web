import { ref } from 'vue'

export function useFetchApiVersion () {
  const version = ref('')
  const loading = ref(false)

  loading.value = true

  fetch(process.env.VUE_APP_API_HOST)
    .then((response) => {
      return response.text()
    })
    .then((value) => {
      version.value = value
      loading.value = false
    })
    .catch((error) => {
      console.log(error)

      loading.value = false
    })

  return { version, loading }
}
