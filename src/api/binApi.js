import { reactive, ref } from 'vue'

export function useCreateBin() {
  const loading = ref(false)
  const error = ref(null)
  const result = reactive({
    slug: '',
    content: ''
  })

  async function createBin (payload) {
    try {
      loading.value = true

      const response = await fetch(`${process.env.VUE_APP_API_HOST}/v1/bins`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(payload)
      })

      const responseData = await response.json()

      result.content = responseData.data.content
      result.slug = responseData.data.slug
    } catch (e) {
      error.value = e
    } finally {
      loading.value = false
    }
  }

  return { result, error, loading, run: createBin }
}

export function useFetchBin() {
  const loading = ref(false)
  const error = ref(null)
  const result = reactive({
    slug: '',
    content: ''
  })

  async function fetchBin (slug) {
    try {
      loading.value = true

      const response = await fetch(`${process.env.VUE_APP_API_HOST}/v1/bins/${slug}`)
      const responseData = await response.json()

      result.content = responseData.data.content
      result.slug = responseData.data.slug
    } catch (e) {
      error.value = e
    } finally {
      loading.value = false
    }
  }

  return { result, error, loading, run: fetchBin }
}
