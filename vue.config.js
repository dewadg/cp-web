const CompressionPlugin = require('compression-webpack-plugin')
const tailwindcss = require('tailwindcss')
const tailwindConfig = require('./tailwind.config')

module.exports = {
  css: {
    loaderOptions: {
      postcss: {
        plugins: [
          tailwindcss(tailwindConfig)
        ]
      }
    }
  },
  configureWebpack: (config) => {
    if (process.env.NODE_ENV === 'production') {
      return {
        plugins: [
          new CompressionPlugin()
        ]
      }
    }

    return {}
  }
}
