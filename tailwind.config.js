module.exports = {
  purge: [
    './src/App.vue',
    './src/components/**/*.vue',
    './src/views/**/*.vue'
  ]
}
